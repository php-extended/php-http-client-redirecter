<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-redirecter library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use PhpExtended\UrlRedirecter\RedirecterInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use Stringable;

/**
 * RedirecterClient class file.
 * 
 * This class is an implementation of a client that handles special cases of
 * redirections that may be more complex than a simple http 30X with Location
 * header.
 * 
 * @author Anastaszor
 */
class RedirecterClient implements ClientInterface, Stringable
{
	
	/**
	 * The inner client.
	 *
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * The redirecter.
	 * 
	 * @var RedirecterInterface
	 */
	protected RedirecterInterface $_redirecter;
	
	/**
	 * Builds a new RedirecterClient with the given inner client.
	 * 
	 * @param ClientInterface $client
	 * @param RedirecterInterface $redirecter
	 */
	public function __construct(ClientInterface $client, RedirecterInterface $redirecter)
	{
		$this->_client = $client;
		$this->_redirecter = $redirecter;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 */
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		while($this->_redirecter->accept($request->getUri()))
		{
			try
			{
				$redirects = $this->_redirecter->getRedirections($request->getUri());
			}
			catch(RuntimeException $exc)
			{
				// this means that the redirecter is not able to get any redirections
				// and we just forward the call to the real client
				break;
			}
			
			foreach($redirects as $redirectedUri)
			{
				$request = $request->withUri($redirectedUri, false);
				continue 2;
				// just ignore secondary links that the redirecter may have given
			}
			// safely breaks in case the redirecter gets no valid redirections
			// and just forwards the call to the real client
			break;
		}
		
		return $this->_client->sendRequest($request);
	}
	
}
