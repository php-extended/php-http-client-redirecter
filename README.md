# php-extended/php-http-client-redirecter

A psr-18 compliant client that handles special cases of redirections

![coverage](https://gitlab.com/php-extended/php-http-client-redirecter/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-http-client-redirecter/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-http-client-redirecter ^8`


## Basic Usage

This library is to make a man in the middle for http requests and responses that
intercepts certain requests it can handle and transforms its destination based
on known rules of redirecters. This is done to avoid multiple http requests with
status code 30X ; or more complex requests with meta http redirect or body and
script redirects.

```php

/* @var $client Psr\Http\Client\ClientInterface */    // psr-18
/* @var $request Psr\Http\Message\RequestInterface */ // psr-7

$client = new RedirecterClient($client);
$response = $client->sendRequest($request);

/* @var $response Psr\Http\Message\ResponseInterface */

```

This library uses the specfic `X-Php-Follow-Location` Header set to zero,
that disables the php setting to follow `Location` headers, if supported by
the http client.


## License

MIT (See [license file](LICENSE)).
