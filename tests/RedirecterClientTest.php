<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-redirecter library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\RedirecterClient;
use PhpExtended\UrlRedirecter\RedirecterInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;

/**
 * RedirecterClientTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\RedirecterClient
 *
 * @internal
 *
 * @small
 */
class RedirecterClientTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var RedirecterClient
	 */
	protected RedirecterClient $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new RedirecterClient(
			$this->getMockForAbstractClass(ClientInterface::class),
			$this->getMockForAbstractClass(RedirecterInterface::class),
		);
	}
	
}
